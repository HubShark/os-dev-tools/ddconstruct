#!/bin/sh

###############################################################################
#    (C) 2013 hyperclock (aka Jimmy M. Coleman)
#	 website:  www.hubshark.com
#	 email:    hyperclock(at)hubshark(dot)com
###############################################################################
###############################################################################
#    BuildForge - Scripts designed to build the HubShark OS, utilizing 
#	 Debian(and/or Devuan) GNU/Linux as a base-.
#
#    BuildForge is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BuildForge is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy (see COPYING) of the GNU General 
#    Public License along with BuildForge.  If not, see 
#    <http://www.gnu.org/licenses/>.
#
################################################################################

## =========================
## VARIABLES
## Change to match your needs
## ==========================
OWNER="www-data"
DEBMLOG="/opt/deb-mirror/debmirror.log"
MIRRORDIR="/opt/deb-mirror"
CODE_NAME="stretch"
BANDWIDTH="1500"
PUBLIC="www-data"
R_OPTIONS="-aIL --partial" # --bwlimit=$BANDWIDTH"

## ========================================
## Extra options that can be added manually 
## =?======================================
#   --omit-suite-symlinks \
#   --no-check-gpg \
#   --ignore-missing-release \


## ==================
## Setup/Test Logging
## ==================
if test -s $DEBMLOG
then
test -f $DEBMLOG.3.gz && mv $DEBMLOG.3.gz $DEBMLOG.4.gz
test -f $DEBMLOG.2.gz && mv $DEBMLOG.2.gz $DEBMLOG.3.gz
test -f $DEBMLOG.1.gz && mv $DEBMLOG.1.gz $DEBMLOG.2.gz
test -f $DEBMLOG.0 && mv $DEBMLOG.0 $DEBMLOG.1 && gzip $DEBMLOG.1
mv $DEBMLOG $DEBMLOG.0
cp /dev/null $DEBMLOG
chmod 640 $DEBMLOG
fi

## ============================
## Record the current date/time
## ============================
date 2>&1 | tee -a $DEBMLOG

## ==============
## Debian General
## ==============
echo "\n*** Debian General ***\n" 2>&1 | tee -a $DEBMLOG
debmirror --i18n --method=rsync --progress --postcleanup --source \
--host=ftp.de.debian.org \
--rsync-options "${R_OPTIONS}" \
$MIRRORDIR/debian/ \
--arch="i386,amd64" \
--dist="$CODE_NAME,${CODE_NAME}-updates,${CODE_NAME}-backports" \
--root=":debian/" \
--section="main,main/debian-installer,contrib,non-free" \
--diff="none" \
--ignore-small-errors 
2>&1 | tee -a $DEBMLOG

# Extract the lists so awk can handle them
find $MIRRORDIR/debian/dists/ -name Packages.bz2 -exec bunzip2 -fk {} \;
find $MIRRORDIR/debian/dists/ -name Sources.bz2 -exec bunzip2 -fk {} \;

## ===============
## Debian Security
## ===============
echo "\n*** Debian Security ***\n" 2>&1 | tee -a $DEBMLOG
debmirror --i18n --method=rsync --progress --postcleanup --source \
--host=security.debian.org \
--rsync-options "${R_OPTIONS}" \
$MIRRORDIR/debian-sec/ \
--arch="i386,amd64" \
--dist="${CODE_NAME}/updates" \
--root=":debian-security/" \
--diff="none" \
--ignore-small-errors 
2>&1 | tee -a $DEBMLOG

# Extract the lists so awk can handle them
find $MIRRORDIR/debian-sec/dists/ -name Packages.bz2 -exec bunzip2 -fk {} \;
find $MIRRORDIR/debian-sec/dists/ -name Sources.bz2 -exec bunzip2 -fk {} \;

## =================
## Debian Multimedia
## =================
echo "\n*** Debian Multimedia ***\n" 2>&1 | tee -a $DEBMLOG
debmirror --i18 --method=rsync --progress --postcleanup --source \
--host=www.deb-multimedia.org \
--rsync-options "${R_OPTIONS}" \
$MIRRORDIR/debian-mm/ \
--arch="amd64,i386" \
--dist="$CODE_NAME" \
--root=":debian-multimedia/" \
--section="main,non-free" \
--diff="none" \
--ignore-small-errors 
2>&1 | tee -a $DEBMLOG

# Extract the lists so awk can handle them
find $MIRRORDIR/debian-mm/dists/ -name Packages.bz2 -exec bunzip2 -fk {} \;
find $MIRRORDIR/debian-mm/dists/ -name Sources.bz2 -exec bunzip2 -fk {} \;

## =================
## Fix Owner & Perm.
## =================
echo "\n*** Fixing ownership ***\n" 2>&1 | tee -a $DEBMLOG
find $MIRRORDIR -type d -o -type f -exec chown $OWNER:$PUBLIC '{}' \; \
2>&1 | tee -a $DEBMLOG

echo "\n*** Fixing permissions ***\n" 2>&1 | tee -a $DEBMLOG
find $MIRRORDIR -type d -o -type f -exec chmod u+rw,g+r,o+r-w '{}' \; \
2>&1 | tee -a $DEBMLOG

## ===============
## Get Mirror Size
## ===============
echo "\n*** Mirror size ***\n" 2>&1 | tee -a $DEBMLOG
du -hs $MIRRORDIR 2>&1 | tee -a $DEBMLOG

## ============================
## Record the current date/time
## ============================
date 2>&1 | tee -a $DEBMLOG
